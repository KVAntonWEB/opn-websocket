const WebSocket = require("ws");
const server = new WebSocket.Server({
  port: 9090,
});
console.log("START");

let sockets = [];
const db = {
  "notification-item": {
    id: "1",
    title: "Средства успешно выведены",
    time: "2022-02-04T17:00:00",
    icon: "wallet",
    isread: true,
    msg: "Здравствуйте, объявление отклонено. Что делать в этом случае? Объявление ориентировано только на пользователей сообщества, кто в курсе о проекте.",
  },
  "chat-item": {
    me: false,
    avatar: "/images/profile-opn.svg",
    username: "OPN Partner",
    date: "2021-10-17",
    msg: "В настоящий момент вижу, что ваше объявление запущено.",
  },
  "chat-list-item": {
    id: "39",
    title: "Помогите выплата нужна))",
    isread: false,
    lastmsg: {
      username: "Anton Test 2",
      avatar:
        "https://control.opnpartner.ru/upload/main/d5b/la574hpfgtbd1px7yhkbl3uxvanldtkh.png",
      date: "2022-02-14T22:00:23+03:00",
    },
  },
  "wallet-data": {
    transactionsAmount: 1,
    paymentsAmount: 0,
    balance: 10,
    frozen: 0,
    forecast: {
      month: null,
      value: 0,
    },
    transactions: [
      {
        id: "1",
        userId: "user1",
        productId: "1",
        user: "Владимир С.",
        title: "Зачисление средств по сделке № 27618",
        sum: 3100,
        isDebet: true,
        level: 1,
        product: "Продажа новостроек",
        icon: "building",
        date: "2021-12-27",
      },
      {
        userId: "user1",
        productId: "1",
        user: "Владимир С.",
        title: "Зачисление средств по сделке № 27618",
        sum: 1000,
        isDebet: false,
        level: 1,
        product: "Продажа новостроек",
        date: "2021-12-27",
      },
    ],
    payments: [
      {
        day: "2021-09-02",
        paymentData: [
          {
            id: 43872,
            title: "Вывод средств на расчётный счёт",
            sum: 30000,
            icon: "ancient-building",
            status: "check",
          },
        ],
      },
      {
        day: "2021-10-14",
        paymentData: [
          {
            id: 43873,
            title: "Вывод средств на расчётный счёт",
            sum: 30000,
            icon: "ancient-building",
            status: "check",
          },
          {
            id: 43874,
            title: "Вывод средств на Webmoney",
            sum: 340000,
            icon: "webmoney",
            status: "stop",
          },
        ],
      },
    ],
  },
  invite: {
    link: "https://opn.group/erp/register/q2456ds_kw",
    count: 13,
  },
};

server.on("connection", function (socket) {
  console.log("connection");
  sockets.push(socket);

  // When you receive a message, send that message to every socket.
  socket.on("message", function (msg) {
    const data = JSON.parse(msg);
    console.log("message", data);
    if (data?.type === "subscribe_events") {
      console.log("type: ", data?.type);
      const value = { ...db[data?.data_type] };
      if (data?.payload) {
        switch (data?.data_type) {
          case "notification-item":
          case "chat-item":
            value.msg += JSON.stringify(data.payload);
            break;
          default:
        }
      }
      const sendData = {
        namespace: data?.namespace,
        mutation: data?.mutation,
        value,
      };
      setTimeout(() => {
        console.log("send: ", sendData);
        socket.send(JSON.stringify(sendData));
      }, 3000);
    }

    // sockets.forEach((s) => s.send(msg));
  });

  // When a socket closes, or disconnects, remove it from the array.
  socket.on("close", function () {
    console.log("close");
    sockets = sockets.filter((s) => s !== socket);
  });
});
